Results for the Software Interview Problems
===

# Problem - 1

- C++ Docker Image

```
Successfully built 3c42c89375a9
```

-  Python Docker Image

```
Successfully built e74db3ae2687
```

- Java Docker Image

```
Successfully built 361e6678950b
```

# Problem - 2

- C++ Build

```
g++ -o /usr/src/target/lru_cpp src/lru_cpp.cpp
```

- Java Build

```
Build Success
```

- Python Run

```
Python run was a Success
```

# Problem - 3

- C++ Docker Run Output

```
5 4 1 3
```

- Java Docker Run Output

```
5 4 1 3
```

- Python Docker Run Output

```
5 4 1 3
```
