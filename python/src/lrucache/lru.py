class LRUCache:
    def __init__(self, capacity):
        self.capacity = capacity
        # LRUCache
        self.cache = []
        # Might not be necessary
        self.lru = set()

    def refer(self, key):
        # not present in cache
        if not key in self.cache :
            # Cache is full
            if len(self.cache) is self.capacity :
                # Remove element that has been in longest
                self.lru.remove(self.cache.pop(0))
        # present in cache
        else :
            self.cache.remove(key)
        # update reference
        self.lru.add(key)
        self.cache.append(key)

    def display(self):
        for i in reversed(self.cache):
            print(i, end=' ')
        print('')
