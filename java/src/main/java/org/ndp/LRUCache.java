package org.ndp;

import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;

public class LRUCache {

    // store keys of cache
    private Deque<Integer> dq;

    // store references of key in cache
    private HashSet<Integer> set;

    //maximum capacity of cache
    private int CSIZE;

    public LRUCache(int n)
    {
        dq=new LinkedList<Integer>();
        set=new HashSet<Integer>();
        CSIZE=n;
    }

    public void refer(int x)
    {
        /* not present in cache
         * cache is full */
    	if(!dq.contains(x) && dq.size() == CSIZE) {
    		// Remove from Set and Deque
    		set.remove(dq.pollLast());
    	}
        // present in cache
    	else {
    		dq.remove(x);
    	}
        // update reference
    	set.add(x);
    	dq.push(x);
    }
    
    public int getCSIZE() {
    	return CSIZE;
    }
    public Deque<Integer> getDQ() {
    	return dq;
    }
    // display contents of cache
    private void display()
    {
        for (Integer integer : dq) {
            System.out.print(integer + " ");
        }
        System.out.println("");
    }

    public static void main(String[] args) {
        LRUCache ca=new LRUCache(4);
        ca.refer(1);
        ca.refer(2);
        ca.refer(3);
        ca.refer(1);
        ca.refer(4);
        ca.refer(5);
        ca.display();
    }

}
